import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:vendetodo_app/providers/productos_provider.dart';

import 'view/view.dart';

void main() => runApp(const AppState());

class AppState extends StatelessWidget {
  const AppState({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (context) => ProductsProvider(),
          lazy: false,
        )
      ],
      child: const MyApp(),
    );
  }
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      color: Colors.white,
      debugShowCheckedModeBanner: false,
      title: 'VendeTodo',
      initialRoute: 'home',
      routes: {
        'home': (_) => const HomeScreen(),
      },
      theme:
          ThemeData(scaffoldBackgroundColor: Colors.white, fontFamily: 'Nova'),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:vendetodo_app/providers/productos_provider.dart';
import 'package:vendetodo_app/widgets/widgets.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final productsProvider = Provider.of<ProductsProvider>(context);

    print(productsProvider.onDisplayProducts);

    return Scaffold(
      body: ListView(
        children: [
          const HomeAppBar(),
          Container(
            //Height temporal
            // height: 500,
            padding: const EdgeInsets.only(top: 15),
            decoration: const BoxDecoration(
              color: Color(0xFFEDECF2),
              // borderRadius: BorderRadius.only(
              //   topLeft: Radius.circular(35),
              //   topRight: Radius.circular(35),
              // ),
            ),
            child: Column(children: [
              // Container(
              //   margin: const EdgeInsets.symmetric(horizontal: 15),
              //   padding: const EdgeInsets.symmetric(horizontal: 15),
              //   height: 50,
              //   decoration: BoxDecoration(
              //       color: Colors.white,
              //       borderRadius: BorderRadius.circular(30)),
              //   child: Row(
              //     children: [
              //       Container(
              //         margin: const EdgeInsets.only(left: 5),
              //         height: 50,
              //         width: 300,
              //         child: TextFormField(
              //           decoration: const InputDecoration(
              //             border: InputBorder.none,
              //             hintText: 'Search here...',
              //           ),
              //         ),
              //       ),
              //       const Spacer(),
              //       const Icon(
              //         Icons.search,
              //         size: 27,
              //         color: Color.fromARGB(255, 255, 0, 0),
              //       ),
              //     ],
              //   ),
              // ),
              // Container(
              //   alignment: Alignment.centerLeft,
              //   margin:
              //       const EdgeInsets.symmetric(vertical: 20, horizontal: 10),
              //   child: const Text(
              //     'Categories',
              //     style: TextStyle(
              //       fontSize: 25,
              //       fontWeight: FontWeight.bold,
              //       color: Color(0xFF4C53A5),
              //     ),
              //   ),
              // ),

              //*Categorias Widget
              // const CategoriesWidget(),
              //*Items
              // Container(
              //   alignment: Alignment.centerLeft,
              //   margin:
              //       const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
              //   child: const Text(
              //     'Nuevos Productos',
              //     style: TextStyle(
              //       fontSize: 25,
              //       fontWeight: FontWeight.bold,
              //       color: Colors.black,
              //     ),
              //   ),
              // ),
              //*Items block
              ItemsWidget(
                products: productsProvider.onDisplayProducts,
                onNextPage: () => productsProvider.getOnDisplayProducts(),
              ),
            ]),
          ),
        ],
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:vendetodo_app/models/models.dart';

class ProductsProvider extends ChangeNotifier {
  List<Producto> onDisplayProducts = [];
  int _popularPage = 0;
  ProductsProvider() {
    getOnDisplayProducts();
  }

  getOnDisplayProducts() async {
    _popularPage++;
    final url = Uri.parse('http://10.0.2.2:3001/api/products/existenciapp');
    final response = await http.post(url, body: {"pagina": "$_popularPage"});
    final productResponse = ProductoResponse.fromJson(response.body);
    onDisplayProducts = [...onDisplayProducts, ...productResponse.data];
    notifyListeners();
  }
}

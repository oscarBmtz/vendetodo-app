import 'dart:convert';

class Producto {
  Producto({
    required this.idProducto,
    required this.nombre,
    required this.descripcion,
    required this.precio,
    required this.disponible,
    required this.imageUrl,
  });

  int idProducto;
  String nombre;
  String descripcion;
  int precio;
  int disponible;
  String imageUrl;

  get imageProduct {
    if (imageUrl != null) return 'http://10.0.2.2:3001$imageUrl';

    return 'https://i.stack.imgur.com/GNhxO.png';
  }

  factory Producto.fromJson(String str) => Producto.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory Producto.fromMap(Map<String, dynamic> json) => Producto(
        idProducto: json["id_producto"],
        nombre: json["nombre"],
        descripcion: json["descripcion"],
        precio: json["precio"],
        disponible: json["disponible"],
        imageUrl: json["image_url"],
      );

  Map<String, dynamic> toMap() => {
        "id_producto": idProducto,
        "nombre": nombre,
        "descripcion": descripcion,
        "precio": precio,
        "disponible": disponible,
        "image_url": imageUrl,
      };
}

// To parse this JSON data, do
//
//     final productoResponse = productoResponseFromMap(jsonString);

import 'dart:convert';

import 'package:vendetodo_app/models/producto.dart';

class ProductoResponse {
  ProductoResponse({
    required this.isValid,
    required this.data,
  });

  bool isValid;
  List<Producto> data;

  factory ProductoResponse.fromJson(String str) =>
      ProductoResponse.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory ProductoResponse.fromMap(Map<String, dynamic> json) =>
      ProductoResponse(
        isValid: json["isValid"],
        data: List<Producto>.from(json["data"].map((x) => Producto.fromMap(x))),
      );

  Map<String, dynamic> toMap() => {
        "isValid": isValid,
        "data": List<dynamic>.from(data.map((x) => x.toMap())),
      };
}

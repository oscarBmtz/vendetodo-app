import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class HomeAppBar extends StatelessWidget {
  const HomeAppBar({Key? key}) : super(key: key);

  _launchUrl(String url) async {
    final uri = Uri(scheme: "https", host: url);
    if (!await launchUrl(uri, mode: LaunchMode.externalNonBrowserApplication)) {
      throw 'Noe puede lanzar url';
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      padding: const EdgeInsets.all(25),
      child: Row(
        children: [
          const FadeInImage(
            width: 45,
            height: 45,
            placeholder: AssetImage('images/jar-loading.gif'),
            image: AssetImage('images/vendetodopng.png'),
          ),
          // const Icon(
          //   Icons.sort,
          //   size: 30,
          //   color: Color.fromARGB(255, 255, 0, 0),
          // ),
          const Padding(
            padding: EdgeInsets.only(left: 20),
            child: Text(
              'Catalogo VendeTodo',
              style: TextStyle(
                fontSize: 23,
                fontWeight: FontWeight.bold,
                color: Colors.black87,
              ),
            ),
          ),
          const Spacer(),
          Badge(
            badgeColor: const Color.fromARGB(255, 3, 66, 107),
            padding: const EdgeInsets.all(7),
            badgeContent: const Text(
              '?',
              style: TextStyle(
                color: Colors.white,
              ),
            ),
            child: InkWell(
              onTap: () {
                _launchUrl('www.google.com');
              },
              child: const Icon(
                Icons.travel_explore,
                size: 35,
                color: Colors.black,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
